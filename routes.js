const { Router } = require('express')

const routes = Router()

routes.get('/', (req, res) => {
  res.sendFile(__dirname + '/views/index.html')
})

routes.get('/sobre', (req, res) => {
  res.sendFile(__dirname + '/views/sobre.html')
})

routes.get('/contato', (req, res) => {
  res.sendFile(__dirname + '/views/contato.html')
})

routes.post('/confirmacao', (req, res) => {
  const { name, email, message } = req.body

  res.status(200).send(
    `
    <div
      style="width: 100%; height: 90vh; display: flex;
        align-items: center; justify-content: center; flex-direction: column;
        font-family: sans-serif;
      "
    >
      <p style="font-size: 24px">
        Obrigado ${name} por ter enviado a mensagem "<cite>${message}</cite>".
        Retornaremos no e-mail ${email}
      </p>
      <a href="/" style="font-size: 22px"
        >Clique aqui para voltar a tela inicial
      </a>
    </div>
    `
  )
})

module.exports = routes
